<h2>Exámen de conocimientos en PHP</h2>
<p>Es un test de elecciones múltiples</p>
<ul>
  <li><strong>Número de preguntas:</strong> <?php echo $count_questions; ?></li>
  <li><strong>Tipo:</strong> Elecciones múltiples</li>
  <li><strong>Tiempo Estimado:</strong> <?php echo $count_questions * .5 ?> Minuto(s) por pregunta</li>
</ul>
<a href="<?php echo site_url('quiz/question/1'); ?>" class="btn btn-outline-primary">Empezar</a>