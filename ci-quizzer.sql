-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2020 a las 02:09:18
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ci-quizzer`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `choices`
--

CREATE TABLE `choices` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT 0,
  `choice_text` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `choices`
--

INSERT INTO `choices` (`id`, `question_id`, `is_correct`, `choice_text`) VALUES
(1, 1, 1, 'name'),
(2, 1, 0, 'file'),
(3, 1, 0, 'action'),
(4, 1, 0, 'descripcion'),
(5, 1, 0, 'valid'),
(6, 2, 0, 'if (a=0) print a'),
(7, 2, 1, 'if (a==0) echo “hola mundo”;'),
(8, 2, 0, 'if (a==0) { echo ok }'),
(9, 2, 0, 'if (b==0) { echo ok }'),
(20, 3, 0, 'hbhbh'),
(21, 3, 1, 'hbhbh'),
(22, 3, 0, 'jhbhjb'),
(23, 3, 0, 'fgf'),
(24, 3, 0, 'fgf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `question_text` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`question_id`, `question_text`) VALUES
(1, '¿En qué atributo de un formulario especificamos la página a la que se van a enviar los datos del mismo?'),
(2, '¿Cuál de estas instrucciones está correctamente escrita en PHP?'),
(3, '¿En qué lugar se ejecuta el código PHP?');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
